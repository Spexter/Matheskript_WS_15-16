# Mathematikskript Wintersemester 2015/16

Hier entsteht ein mitschrieb fuer die Vorlesung bei ''Britta Dorn''
Um das Skript zu erhalten braucht ihr Tex fuer [Linux](https://tug.org/texlive/), [Mac](https://tug.org/mactex/) oder [Windows](https://tug.org/protext/). 




* Falls ihr mit _git_ arbeiten wollt: 
```
    git clone https://gitlab.com/Spexter/Matheskript_WS_15-16.git
    cd Matheskript_WS_15-16
    pdflatex mainmathe.tex
```
Jetzt solltet ihr eine mainmathe.pdf (und andere Nebenprodukte von Tex haben)

* Falls ihr nur mit GUI arbeiten wollt:
  * Ladet euch die die `.zip` herunter
  * oeffnet die `mathemain.tex`
  * drueckt auf `PDFTex` oder aehnliches in der Menuzeile
  
  Falls ihr nur die PDF wollt hier klicken:
https://drive.google.com/file/d/0B1dZ79uop9hfRmJ0WjNSbG1MU0k/view?usp=sharing
  
  ist immer aktuell.